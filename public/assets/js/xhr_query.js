/**
 * @file assets/js/xhr_query.js
 * 
 * @description Creates xhr request then reads and stores the response as json
 * 
 * @author norbert.papp <papp.norbert@tarhelypark.hu>
 * @copyright Norbert Papp
 * @license #
 * @version 0.0.1
 */

/**
 * @function ajaxGetRemoteJson()
 * @param {String} url 
 * @param {Function} callback 
 */

function ajaxGetRemoteJson(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4 && xhr.status == 200) {
      // for testing
      // console.log('responseText:' + xhr.responseText);
      try {
        var data = JSON.parse(xhr.responseText);
      } catch (err) {
        console.log(err.message + " in " + xhr.responseText);
        return;
      }
      callback(data);
    }
  };

  xhr.open("GET", url, true);
  xhr.send();
}

/**
 * @function insertIP()
 * @param {Object} obj 
 * @param {String} elementID 
 */

function insertIP(obj, elementID) {
  document.getElementById(elementID).innerHTML = obj.ip;
}

/** 
 * Getting visitor IP via the visitor_ip.php & inserting into the specific html element
 * It can be tested with https://ip4.seeip.org/json if the script is on localhost
 */

let debugIP = "https://ip4.seeip.org/json";
let localIP = "./functions/visitor_ip.php";
ajaxGetRemoteJson(localIP, function(data) {
  document.getElementById("ipaddress").innerHTML = data.ip;
});