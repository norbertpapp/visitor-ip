<?php

/**
 * @file functions/visitor_ip.php
 * 
 * @description Set JSON headers and return visitor's IP in JSON format
 * 
 * @author norbert.papp <papp.norber@tarhelypark.hu>
 * @copyright Norbert Papp
 * @license #
 * @version 0.0.1
 */

/* Setting JSON headers */
function set_json_headers() {
  header('Content-type: application/json');
  header('Access-Control-Allow-Origin: ');
}

function get_user_ip() {
  /* Getting visitor IP */
  $ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
  return json_encode(array('ip' => $ip));
}

set_json_headers();
echo get_user_ip();

?>